const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  //配置服务器域名与端口号
  devServer:{
    host:'127.0.0.1',
    port:8080,
    proxy: {
      '/api': {
        target: ' http://gmall-h5-api.atguigu.cn',
        pathRewrite: { '^/api': '' },
      },
    },
  },
  //关闭eslint校验工具
  lintOnSave:false
})
