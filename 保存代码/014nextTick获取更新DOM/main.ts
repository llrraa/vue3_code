//第一步:引入vue3提供createApp创建应用实例函数
//没有构造函数Vue,导致没有VM[$bus]
import {createApp} from 'vue';
//第二步:引入根组件
import App from './App.vue';
//第三步:创建应用实例
let app = createApp(App);


//引入对话框组件
import Dialog from './components/Dialog.vue';
//全局组件
app.component(Dialog.name,Dialog);

//第四步:将应用挂载到挂载点上
app.mount('#app');



