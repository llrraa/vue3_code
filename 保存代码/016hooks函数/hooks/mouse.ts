import { ref, onMounted } from 'vue';
//复用逻辑函数
export const mouseHooks = () => {
    //鼠标位置
    let x = ref(0);
    let y = ref(0);
    //当组件挂载完毕会出发
    onMounted(() => {
        window.addEventListener('mousemove', (event) => {
            x.value = event.pageX;
            y.value = event.pageY;
        })
    })
    //对外暴露鼠标位置数据
    return { x, y }
}