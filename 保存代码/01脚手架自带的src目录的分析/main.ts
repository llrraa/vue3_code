//次代码:从vue3核心代码中引入createApp函数
//次函数可以创建应用的实例
import { createApp } from 'vue'
//引入App更组件
import App from './App.vue'
//通过vue3提供createApp方法创建一个应用实例
//第一个参数:根组件
let app  = createApp(App);
//把应用挂载到挂载点上
app.mount('#app');//mount挂载应用,应用实例还有一个方法unmount卸载
//注意:以后查看开发者工具的时候，App顶部没有root[VM]
//在入口文件，构造函数Vue都是没有,VM就没有了！！！！
//vue3当中$bus全局事件总线没了
//vue.component(),全局组件这种写法没了





