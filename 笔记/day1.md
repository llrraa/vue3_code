最近的安排:
vue3基础 4天时间
中台项目 8-10天
微信小程序......












let a:number = 100;
console.log(a);
//枚举
enum url{
    login="http://www.baidu.com",
    logout="http://www.sina.com",
    user="http://www.erha.com"
} 
console.log(url)


//接口:定义对象类型
interface People{
   name:string|number|boolean,
   age:number
   id?:number
}

//泛型:约束函数形参的类型|返回值的类型
function ref<T>(a:T,b:T):T{
   return a;
}

ref<string>('我爱你',"塞北的雪");
ref<number>(100,200)



一、vue3简介
官网地址:https://cn.vuejs.org/
将来进入公司开发vue3: vue3 + vite + pinia + vue-router + ts  技术栈
尤雨溪:2020年09月18日发布Vue3最新版本！！！！

Vue2和Vue3之间的区别是什么？
--vue3兼容99%vue2语法
--Teleport|Suspense这些都是Vue3新增的内置组件
--vue2一个组件模板只能有一个根标签|Vue3当中一个组件模板可以有多个根标签！！
--vue3新增一个特性,组合式API[都是函数]
--vue3重写虚拟DOM，底层代码进行优化




二、vue-cli初始化Vue3项目(脚手架)
vue create 项目的名字
选择第三个 [包含ts]
运行:npm run serve

2.1让浏览器自动的打开
2.2关闭eslint


三、分析vue-cli脚手架目录与源代码
node_modules:放置项目依赖
public:放置静态资源的[静态页面 + 小图标],在项目打包的时候这些静态资源原封不动dist文件及里面
src:程序员源代码区域
.d.ts声明式文件:在写项目的时候，这个文件不会用，不操心！！！
这类ts文件,一般会出现ts一个关键字declare:声明变量、声明ts类型type、接口
可以在其他ts文件当中直接使用不需要引入！！！！




四、vue3是否完全兼容vue2?
经过测试，vue3确实兼容大多数vue2的语法。
Vue3抛弃钩子: beforeDestroy|destroyed



五、组合式API之setup函数
setup:安装
---函数体内没有this
---setup函数不能使用async
---setup函数体返回的对象的属性|方法可以提供给模板|某些函数体内组件实例提供使用
---setup目前可以理解为data|methods结合体





六、组合式API之ref函数
提示插件:vue-vscode-snippets插件
6.1深入学习ref组合式API


七、组合式API之reactive函数






八、setup使用的细节






