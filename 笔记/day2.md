一、今天重点
---计算属性computed|监听watch
--toRefs
--生命周期函数变化
--hooks

面试题:Vue3新增了哪些新增的特性?
--新增组合式API:setup|ref|reactive|watch|computed|onMounted
--组件可以有多个根节点
--新增一些内置组件Teleport|Suspense


面试题:Vue2与Vue3有哪些区别?
--新增组合式API:setup|ref|reactive|watch|computed|onMounted
--组件可以有多个根节点
--新增一些内置组件Teleport|Suspense
--Vue3兼容大多与Vue2语法,抛弃生命周期函数销毁阶段
--相应数据原理区别:Object.defineProperty(IE8)|Proxy(IE11)


面试题:Object.defineProperty|Proxy 区别?
---兼容性不同(Object.definePropertyIE8以上) Proxy(IE全部兼容)
--Object.defineProperty只能监听某一个对象的某一个属性的变化,
--Proxy:监听整个对象的变化



面试题:请自我介绍?
---目的性:为什么离职
---突显能干什么、曾经做过什么



一、复习一下

Vue3新增组合式API函数:setup|ref|reactive





二、ref
ref:(一般)定义基本类型数据达到响应式目的





三、组合式API函数之计算属性computed
当年Vue2语法:选择式API
export default {
     data(){
         return {}
     },
     watch:{

     },
     computed:{

     }
}


//组合式API
import {ref,reactive,computed,watch} from 'vue';
export default {
     setup(){
         return {
             name:'xxxx'
         }
     }
}





四、组合式API函数之watch！！！！





五、watchEffect组合式API?



六、组合式API函数之toRefs


七、获取DOM元素或者组件实例?




八、组合式API函数之生命周期函数
V2:钩子函数        V3:销毁阶段不要了
beforeCreate
created
beforeMount
mounted 
beforeUpdate
updated
beforeDestroy
destroyed


V3组合式API函数之生命周期函数
--beforeCreate           setup
--created                onBeforeMount
                         onMounted

                         onBeforeUpdate
                         onUpdated
                         
                         onBeforeUnmount
                         onUnmounted




